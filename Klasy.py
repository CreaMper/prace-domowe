class LivingCreature(object):
        pass
living_creature = LivingCreature()
print(living_creature)
print(isinstance(living_creature, LivingCreature))
print(isinstance(living_creature, object))

class Animal(LivingCreature):
        pass

class Plant(LivingCreature):
        pass
animal = Animal()
plant = Plant()

print(animal)
print(plant)

print(isinstance(animal, Animal))
print(isinstance(plant, Plant))

print(isinstance(animal, Plant))
print(isinstance(plant, Animal))

print(isinstance(animal, LivingCreature))
print(isinstance(plant, LivingCreature))

print(isinstance(animal, object))
print(isinstance(plant, object))


